# !!!!!!!!!!!!! IMPORTANT !!!!!!!!!!!!!
# before this is run the binary must be built
# see Makefile task 'release'
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

FROM debian:8

# sql uses SSL and requires ca-certs.
RUN apt update
RUN apt install -y ca-certificates

ADD bin/$$SERVICE_NAME$$-linux-amd64 /app

ENV API $$SERVICE_NAME$$

EXPOSE 3000

CMD ["/app"]

