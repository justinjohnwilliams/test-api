# helper method to recursively search for files
rwildcard=$(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2) $(filter $(subst *,%,$2),$d))

# include vendor files
SRC_FILES:=$(call rwildcard,,*.go)

RM:=rm -rf

# build the native bin
bin/lead: $(SRC_FILES)
	govendor build -o bin/$$SERVICE_NAME$$ .

# run gometalinter and check for errors
lint: $(SRC_FILES)
	@gometalinter --vendor --disable-all --enable=vet \
		--enable=vetshadow --enable=gotype --enable=golint \
		--enable=ineffassign --enable=goconst --enable=errcheck \
		--enable=deadcode --enable=varcheck --enable=unconvert \
		--enable=goconst --enable=gosimple --enable=dupl .
.PHONY: lint

# run the application natively
run: bin/$$SERVICE_NAME$$
	$< -env-file dev.env.list
.PHONY: run

# run go tests
test:
	@govendor test +local -v
.PHONY: test

# delete the created bins
clean:
	$(RM) bin/
.PHONY: clean

# make the linux-amd64 bin to be used with docker
release: bin/$$SERVICE_NAME$$-linux-amd64
bin/$$SERVICE_NAME$$-linux-amd64: $(SRC_FILES)
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 govendor build -o $@ .
.PHONY: release

# build the docker image
docker: bin/$$SERVICE_NAME$$-linux-amd64
	@docker build . -t registry.gitlab.com/onetech/$$REPOSITORY_NAME$$:develop
.PHONY: docker

# run the application in docker
dockerrun: docker
	@docker run --rm -it --name $$REPOSITORY_NAME$$ \
		--env-file dev.env.list \
		--env "VAULT_ADDR=http://vault:8200" \
		-p 3000:3000 \
		--network composer_default \
		registry.gitlab.com/onetech/$$REPOSITORY_NAME$$:develop
.PHONY: dockerrun

