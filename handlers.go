package main

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/OneTech/log-go"
)

// HTTPErrorHandler is invoked if any http handler returns an error
// it logs the error and returns a 500 with an empty message for production
// or a stack trace/error message in dev
func httpErrorHandler(err error, c echo.Context) {
	cxt := getLogContext(c)

	code := http.StatusInternalServerError
	msg := http.StatusText(code)

	if httpErr, ok := err.(*echo.HTTPError); ok {
		code = httpErr.Code
		msg = fmt.Sprint(httpErr.Message)
	}

	// in debug mode, send all error messages back to the client
	if c.Echo().Debug {
		msg = fmt.Sprintf("%+v", err)
	}

	// If committed is true then the handler has already written a response body for the client
	// otherwise we will write the error message
	if !c.Response().Committed {
		body := map[string]interface{}{
			"error": msg,
		}

		if e := c.JSON(code, body); e != nil {
			log.ErrorContext(fmt.Sprint(e), cxt)
		}
	}

	// log server errors
	if code >= 500 && code < 600 {
		req := c.Request()
		log.ErrorContext(fmt.Sprintf("an error occurred: %s %s: %s", req.Method, req.RequestURI, err), cxt)
	}
}

func getLogContext(c echo.Context) *log.Context {

	cxt := &log.Context{}
	cxt.RequestID = c.Request().Header["X-Request-Id"]
	cxt.CorrelationID = c.Request().Header["X-Correlation-Id"]

	return cxt
}

