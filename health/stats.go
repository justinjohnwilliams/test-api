package health

import (
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"

	"gitlab.com/OneTech/lead-api/data"

	"github.com/labstack/echo"
)

var (
	CurrentStats *Stats
	mutex        sync.RWMutex
)

func init() {
	CurrentStats = NewStats()
}

type (
	Stats struct {
		Uptime       string            `json:"uptime"`
		RequestCount uint64            `json:"requestCount"`
		Hostname     string            `json:"hostname"`
		Statuses     map[string]uint64 `json:"statuses"`
		Pid          int               `json:"pid"`
		OpenDBConns  int               `json:"open_db_conns"`
	}
)

func NewStats() *Stats {
	hn, err := os.Hostname()
	if err != nil {
		hn = "unknown"
	}

	return &Stats{
		Uptime:   time.Now().UTC().Format(time.RFC3339),
		Statuses: make(map[string]uint64),
		Pid:      os.Getpid(),
		Hostname: hn,
	}
}

// Process is the middleware function.
func Process(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		if err := next(c); err != nil {
			c.Error(err)
		}
		mutex.Lock()
		defer mutex.Unlock()
		CurrentStats.RequestCount++
		status := strconv.Itoa(c.Response().Status)
		CurrentStats.Statuses[status]++
		return nil
	}
}

// Handler is the endpoint to get stats.
func Handler(c echo.Context) error {
	mutex.RLock()
	defer mutex.RUnlock()
	db, err := data.Connection()
	if err == nil {
		CurrentStats.OpenDBConns = db.Stats().OpenConnections
	}

	return c.JSON(http.StatusOK, CurrentStats)
}

