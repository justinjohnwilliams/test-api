package main

import (
	"flag"
	"io/ioutil"
	"os"
	"strconv"

	envparse "github.com/blockloop/go-envparse"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	"gitlab.com/OneTech/log-go"
	"gitlab.com/OneTech/$$REPOSITORY_NAME$$/health"
)

var (
	api = echo.New()
)

func init() {
	var conf string
	flag.StringVar(&conf, "env-file", "", "environment file for development mode. Should be in the format of docker env-file.")
	flag.Parse()

	if conf != "" {
		if err := envparse.ParseFile(conf); err != nil {
			log.Fatalf("problem with env-file: %s", err)
		}
	}
}


func main() {
	api.Debug, _ = strconv.ParseBool(os.Getenv("DEBUG"))
	log.Infof("debug: %t", api.Debug)

	log.Info("application started")

	// silence default logger
	api.Logger.SetOutput(ioutil.Discard)
	api.Pre(middleware.RemoveTrailingSlash())

	// MIDDLEWARES
	api.Use(health.Process)
	api.Use(middleware.Gzip())
	api.HTTPErrorHandler = httpErrorHandler
	api.Use(middleware.Recover())
	api.Use(middleware.CORS())

	// Routes
	api.GET("/healthz", health.Handler)

	// init and start the API
	port := os.Getenv("PORT")
	if port == "" {
		port = ":3000"
	} else {
		port = ":" + port
	}

	log.Info("listening at", port)
	if err := api.Start(port); err != nil {
		log.Fatal(err)
	}
	log.Info("application exit")
}
