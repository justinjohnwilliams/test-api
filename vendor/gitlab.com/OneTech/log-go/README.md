# log
```Go
import "gitlab.com/OneTech/log-go"
```
## Usage

```go
var (

	// FatalExit determins if os.Exit(1) should be called on log.Fatal calls. It's really only used for testing.
	// If you're not testing them move along
	FatalExit = true

	// Stderr is the io.Writer to be used for stderr. Don't mess with this, seriously. It's only for testing
	// If you're not testing then move along
	Stderr io.Writer = os.Stderr

	// Stdout is the io.Writer to be used for stdout. Don't mess with this, seriously. It's only for testing
	// If you're not testing then move along
	Stdout io.Writer = os.Stderr
)
```

#### func  Error

```go
func Error(v ...interface{})
```
Error is for anything that is considered not ideal for the application, but can
still be handled

#### func  ErrorContext

```go
func ErrorContext(msg string, ctx *Context)
```
ErrorContext logs an error message with Context

#### func  Errorf

```go
func Errorf(msg string, v ...interface{})
```
Errorf is the fmt.Sprintf version of Error

#### func  Fatal

```go
func Fatal(v ...interface{})
```
Fatal is for things that fundamentally render the application useless. Fatal
logs a message and then performs os.Exit(1)

#### func  FatalContext

```go
func FatalContext(msg string, ctx *Context)
```
FatalContext logs a Fatal message with Context

#### func  Fatalf

```go
func Fatalf(msg string, v ...interface{})
```
Fatalf is the fmt.Sprintf version of Fatal

#### func  Info

```go
func Info(v ...interface{})
```
Info is for informational logs. Nothing bad has occured.

#### func  InfoContext

```go
func InfoContext(msg string, ctx *Context)
```
InfoContext logs an informational message with Context

#### func  Infof

```go
func Infof(msg string, v ...interface{})
```
Infof is the fmt.Sprintf version of Info

#### func  Timer

```go
func Timer(event string, utcStart time.Time)
```
Timer times a function call. It is intended to be used with defer. Example:

    func getProducts() {
      // do some work
      defer log.Timer("db: dbo.GetProducts", time.Now().UTC())
      db.Exec("dbo.GetProducts")
    }

This will measure the time the amount of time it took for getProducts() to
execute

#### func  TimerStart

```go
func TimerStart(event string) (stop func())
```
TimerStart logs a duration for an event. It is similar to DTimer, but you
control when the timer stops Example:

    func getProducts() {
      stop := log.TimerStart("db: dbo.GetProducts")
      db.Exec("dbo.GetProducts")
      stop()
    }

#### type Context

```go
type Context struct {
	RequestID     []string
	CorrelationID []string
}
```
