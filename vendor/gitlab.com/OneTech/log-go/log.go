package log

import (
	"fmt"
	"io"
	"os"
	"runtime"
	"strings"
	"sync"
	"time"
)

var (
	l = &sync.Mutex{}

	// FatalExit determins if os.Exit(1) should be called on log.Fatal calls. It's really only used for testing.
	// If you're not testing them move along
	FatalExit = true

	// Stderr is the io.Writer to be used for stderr. Don't mess with this, seriously. It's only for testing
	// If you're not testing then move along
	Stderr io.Writer = os.Stderr

	// Stdout is the io.Writer to be used for stdout. Don't mess with this, seriously. It's only for testing
	// If you're not testing then move along
	Stdout io.Writer = os.Stderr
)

func init() {
}

const (
	levelInfo  = "INFO"
	levelError = "ERROR"
	levelFatal = "FATAL"
)

type Context struct {
	RequestID     []string
	CorrelationID []string
}

// Info is for informational logs. Nothing bad has occured.
func Info(v ...interface{}) {
	log("INFO", v, nil)
}

// Infof is the fmt.Sprintf version of Info
func Infof(msg string, v ...interface{}) {
	log("INFO", fmt.Sprintf(msg, v...), nil)
}

// InfoContext logs an informational message with Context
func InfoContext(msg string, ctx *Context) {
	log("INFO", msg, ctx)
}

// Error is for anything that is considered not ideal for the application, but can still be handled
func Error(v ...interface{}) {
	log("ERROR", v, nil)
}

// Errorf is the fmt.Sprintf version of Error
func Errorf(msg string, v ...interface{}) {
	log("ERROR", fmt.Sprintf(msg, v...), nil)
}

// ErrorContext logs an error message with Context
func ErrorContext(msg string, ctx *Context) {
	log("ERROR", msg, ctx)
}

// Fatal is for things that fundamentally render the application useless. Fatal logs a message and then performs os.Exit(1)
func Fatal(v ...interface{}) {
	log("FATAL", v, nil)
}

// Fatalf is the fmt.Sprintf version of Fatal
func Fatalf(msg string, v ...interface{}) {
	log("FATAL", fmt.Sprintf(msg, v...), nil)
}

// FatalContext logs a Fatal message with Context
func FatalContext(msg string, ctx *Context) {
	log("FATAL", msg, ctx)
}

func log(lvl, msg interface{}, ctx *Context) {
	w := Stderr
	if lvl == levelInfo {
		w = Stdout
	}

	_, file, line, ok := runtime.Caller(2)
	if ok {
		splits := strings.Split(file, "/")
		file = splits[len(splits)-1]
	} else {
		file = "???"
		line = 0
	}

	now := time.Now().UTC().Format(time.RFC3339)
	l.Lock()
	if ctx == nil {
		fmt.Fprintf(w, "%s: %s %s:%d] %s\n", lvl, now, file, line, msg)
	} else {
		fmt.Fprintf(w, "%s: %s %s:%d requestid:%s corrid:%s] %s\n", lvl, now, file, line, ctx.RequestID, ctx.CorrelationID, msg)
	}
	l.Unlock()

	if lvl == levelFatal && FatalExit {
		os.Exit(1)
	}
}

// Timer times a function call. It is intended to be used with defer.
// Example:
//
//  func getProducts() {
//    // do some work
//    defer log.Timer("db: dbo.GetProducts", time.Now().UTC())
//    db.Exec("dbo.GetProducts")
//  }
// This will measure the time the amount of time it took for getProducts() to execute
func Timer(event string, utcStart time.Time) {
	stop := time.Now().UTC()
	diff := stop.Sub(utcStart)
	log("TIMER", fmt.Sprintf("[%s] (%4.3fs)", event, float64(diff.Nanoseconds())/float64(1000000000)), nil)
}

// TimerStart logs a duration for an event. It is similar to DTimer, but you control when the timer stops
// Example:
//
//  func getProducts() {
//    stop := log.TimerStart("db: dbo.GetProducts")
//    db.Exec("dbo.GetProducts")
//    stop()
//  }
func TimerStart(event string) (stop func()) {
	start := time.Now().UTC()

	return func() {
		stop := time.Now().UTC()
		diff := stop.Sub(start)
		log("TIMER", fmt.Sprintf("[%s] (%4.3fs)", event, float64(diff.Nanoseconds())/float64(1000000000)), nil)
	}
}
